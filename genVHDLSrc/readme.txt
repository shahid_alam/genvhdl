/****************************************************************************
|                                                                            |
|                            Readme file for genVHDL                         |
|                                      by                                    |
|                                  Shahid Alam                               |
|                             salam3@connect.carleton.ca                     |
|                                                                            |
|                                                                            |
*****************************************************************************/

genVHDL [1] generates VHDL code from higher level hardware specifications in XML.
It's written in C++ and currently compiled for Windows platform. But it should be
easily ported to other platforms.

USAGE:

genVHDL [options] <XML file>

This program invokes the SAX2XMLReader, and then generate VHDL
from the data returned by various SAX2 handlers for the specified
XML file.

Options:
    -v=xxx		Validation scheme [always | never | auto*]
    -?			Show this help
    * = Default if not provided explicitly.




REFERENCES:

[1] Shahid Alam and Misbah Islam, "genVHDL: Automatic HDL Code Generation from
    high-level XML based Hardware Specifications", Technical Report OCIECE-07-01,
    Ottawa-Carleton Institute of Electrical and Computer Engineering, January 2007.
