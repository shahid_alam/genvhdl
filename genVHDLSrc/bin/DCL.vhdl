library ieee;
use ieee.std_logic_1164.all;

entity OR_1 is
   port(d0, d1: in std_ulogic;
        z: out std_ulogic
   );
end OR_1;

architecture OR_1a of OR_1 is
begin
   or_process: process(d0, d1) is
   begin
      z <= d0 or d1;
   end process or_process;
end architecture OR_1a;


library ieee;
use ieee.std_logic_1164.all;

entity OR_2 is
   port(d0, d1, d2, d3: in std_ulogic;
        z: out std_ulogic
   );
end OR_2;

architecture OR_2a of OR_2 is
begin
   or_process: process(d0, d1, d2, d3) is
   begin
      z <= ((d0 or d1) or d2) or d3;
   end process or_process;
end architecture OR_2a;


library ieee;
use ieee.std_logic_1164.all;

entity AND_1 is
   port(d0, d1: in std_ulogic;
        z: out std_ulogic
   );
end AND_1;

architecture AND_1a of AND_1 is
begin
   and_process: process(d0, d1) is
   begin
      z <= d0 and d1;
   end process and_process;
end architecture AND_1a;


library ieee;
use ieee.std_logic_1164.all;

entity AND_2 is
   port(d0, d1, d2, d3: in std_ulogic;
        z: out std_ulogic
   );
end AND_2;

architecture AND_2a of AND_2 is
begin
   and_process: process(d0, d1, d2, d3) is
   begin
      z <= ((d0 and d1) and d2) and d3;
   end process and_process;
end architecture AND_2a;


library ieee;
use ieee.std_logic_1164.all;

entity NAND_1 is
   port(d0, d1, d2, d3: in std_ulogic;
        z: out std_ulogic
   );
end NAND_1;

architecture NAND_1a of NAND_1 is
begin
   nand_process: process(d0, d1, d2, d3) is
   begin
      z <= ((d0 nand d1) nand d2) nand d3;
   end process nand_process;
end architecture NAND_1a;


library ieee;
use ieee.std_logic_1164.all;

entity NAND_2 is
   port(d0, d1, d2, d3, d4, d5, d6, d7: in std_ulogic;
        z: out std_ulogic
   );
end NAND_2;

architecture NAND_2a of NAND_2 is
begin
   nand_process: process(d0, d1, d2, d3, d4, d5, d6, d7) is
   begin
      z <= ((((((d0 nand d1) nand d2) nand d3) nand d4) nand d5) nand d6) nand d7;
   end process nand_process;
end architecture NAND_2a;


library ieee;
use ieee.std_logic_1164.all;

entity XOR_1 is
   port(d0, d1, d2, d3: in std_ulogic;
        z: out std_ulogic
   );
end XOR_1;

architecture XOR_1a of XOR_1 is
begin
   xor_process: process(d0, d1, d2, d3) is
   begin
      z <= ((d0 xor d1) xor d2) xor d3;
   end process xor_process;
end architecture XOR_1a;


library ieee;
use ieee.std_logic_1164.all;

entity XOR_2 is
   port(d0, d1: in std_ulogic;
        z: out std_ulogic
   );
end XOR_2;

architecture XOR_2a of XOR_2 is
begin
   xor_process: process(d0, d1) is
   begin
      z <= d0 xor d1;
   end process xor_process;
end architecture XOR_2a;


library ieee;
use ieee.std_logic_1164.all;

entity MUX_1 is
   port(sel: in integer range 0 to 1;
        d0, d1: in std_ulogic_vector(0 downto 0);
        z: out std_ulogic_vector(0 downto 0)
   );
end MUX_1;

architecture MUX_1a of MUX_1 is
begin
   mux_process: process(sel, d0,  d1) is
   begin
      case sel is
         when 0 =>
            z <= d0;
         when 1 =>
            z <= d1;
      end case;
   end process mux_process;
end architecture MUX_1a;


library ieee;
use ieee.std_logic_1164.all;

entity MUX_2 is
   port(sel: in integer range 0 to 7;
        d0, d1, d2, d3, d4, d5, d6, d7: in std_ulogic_vector(7 downto 0);
        z: out std_ulogic_vector(7 downto 0)
   );
end MUX_2;

architecture MUX_2a of MUX_2 is
begin
   mux_process: process(sel, d0,  d1,  d2,  d3,  d4,  d5,  d6,  d7) is
   begin
      case sel is
         when 0 =>
            z <= d0;
         when 1 =>
            z <= d1;
         when 2 =>
            z <= d2;
         when 3 =>
            z <= d3;
         when 4 =>
            z <= d4;
         when 5 =>
            z <= d5;
         when 6 =>
            z <= d6;
         when 7 =>
            z <= d7;
      end case;
   end process mux_process;
end architecture MUX_2a;


library ieee;
use ieee.std_logic_1164.all;

ENTITY CLA_1 is
   PORT(x_in        : in std_ulogic_vector(3 downto 0);
        y_in        : in std_ulogic_vector(3 downto 0);
        carry_in    : in std_ulogic;
        sum         : out std_ulogic_vector(3 downto 0);
        carry_out   : out std_ulogic
   );
END CLA_1;

ARCHITECTURE CLA_1a of CLA_1 is

signal h_sum               : std_ulogic_vector(3 downto 0);
signal carry_generate      : std_ulogic_vector(3 downto 0);
signal carry_propagate     : std_ulogic_vector(3 downto 0);
signal carry_in_internal   : std_ulogic_vector(3 downto 1);

BEGIN
   h_sum <= x_in XOR y_in;
   carry_generate <= x_in AND y_in;
   carry_propagate <= x_in OR y_in;

   PROCESS (carry_in, carry_generate, carry_propagate, carry_in_internal)
   BEGIN
      carry_in_internal(1) <= carry_generate(0) OR (carry_propagate(0) AND carry_in);
         inst: FOR i IN 1 TO 2 LOOP
            carry_in_internal(i+1) <= carry_generate(i) OR (carry_propagate(i) AND carry_in_internal(i));
         END LOOP;
   carry_out <= carry_generate(3) OR (carry_propagate(3) AND carry_in_internal(3));
   END PROCESS;

   sum(0) <= h_sum(0) XOR carry_in;
   sum(3 DOWNTO 1) <= h_sum(3 DOWNTO 1) XOR carry_in_internal(3 DOWNTO 1);
END architecture CLA_1a;


library ieee;
use ieee.std_logic_1164.all;

ENTITY CLA_2 is
   PORT(x_in        : in std_ulogic_vector(7 downto 0);
        y_in        : in std_ulogic_vector(7 downto 0);
        carry_in    : in std_ulogic;
        sum         : out std_ulogic_vector(7 downto 0);
        carry_out   : out std_ulogic
   );
END CLA_2;

ARCHITECTURE CLA_2a of CLA_2 is

signal h_sum               : std_ulogic_vector(7 downto 0);
signal carry_generate      : std_ulogic_vector(7 downto 0);
signal carry_propagate     : std_ulogic_vector(7 downto 0);
signal carry_in_internal   : std_ulogic_vector(7 downto 1);

BEGIN
   h_sum <= x_in XOR y_in;
   carry_generate <= x_in AND y_in;
   carry_propagate <= x_in OR y_in;

   PROCESS (carry_in, carry_generate, carry_propagate, carry_in_internal)
   BEGIN
      carry_in_internal(1) <= carry_generate(0) OR (carry_propagate(0) AND carry_in);
         inst: FOR i IN 1 TO 6 LOOP
            carry_in_internal(i+1) <= carry_generate(i) OR (carry_propagate(i) AND carry_in_internal(i));
         END LOOP;
   carry_out <= carry_generate(7) OR (carry_propagate(7) AND carry_in_internal(7));
   END PROCESS;

   sum(0) <= h_sum(0) XOR carry_in;
   sum(7 DOWNTO 1) <= h_sum(7 DOWNTO 1) XOR carry_in_internal(7 DOWNTO 1);
END architecture CLA_2a;


library ieee;
use ieee.std_logic_1164.all;

ENTITY CLA_3 is
   PORT(x_in        : in std_ulogic_vector(15 downto 0);
        y_in        : in std_ulogic_vector(15 downto 0);
        carry_in    : in std_ulogic;
        sum         : out std_ulogic_vector(15 downto 0);
        carry_out   : out std_ulogic
   );
END CLA_3;

ARCHITECTURE CLA_3a of CLA_3 is

signal h_sum               : std_ulogic_vector(15 downto 0);
signal carry_generate      : std_ulogic_vector(15 downto 0);
signal carry_propagate     : std_ulogic_vector(15 downto 0);
signal carry_in_internal   : std_ulogic_vector(15 downto 1);

BEGIN
   h_sum <= x_in XOR y_in;
   carry_generate <= x_in AND y_in;
   carry_propagate <= x_in OR y_in;

   PROCESS (carry_in, carry_generate, carry_propagate, carry_in_internal)
   BEGIN
      carry_in_internal(1) <= carry_generate(0) OR (carry_propagate(0) AND carry_in);
         inst: FOR i IN 1 TO 14 LOOP
            carry_in_internal(i+1) <= carry_generate(i) OR (carry_propagate(i) AND carry_in_internal(i));
         END LOOP;
   carry_out <= carry_generate(15) OR (carry_propagate(15) AND carry_in_internal(15));
   END PROCESS;

   sum(0) <= h_sum(0) XOR carry_in;
   sum(15 DOWNTO 1) <= h_sum(15 DOWNTO 1) XOR carry_in_internal(15 DOWNTO 1);
END architecture CLA_3a;


