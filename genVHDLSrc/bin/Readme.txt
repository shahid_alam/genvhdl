/****************************************************************************
|                                                                            |
|                            Readme file for genVHDL                         |
|                                      by                                    |
|                                  Shahid Alam                               |
|                             salam3@connect.carleton.ca                     |
|                                                                            |
|                                                                            |
*****************************************************************************/

genVHDL [1] generates VHDL code from higher level hardware specifications in XML.
It's written in C++ and currently compiled for Windows platform. But it should be
easily ported to other platforms.

USAGE:

genVHDL [options] <XML file>

This program invokes the SAX2XMLReader, and then generate VHDL
from the data returned by SAX2 handlers for the specified XML file.

Options:
    -v=xxx		Validation scheme [always | never | auto*]
    -o=xxx		Name of VHDL file to be created [DCL.vhdl*]
    -?			Show this help

    * = Default if not provided explicitly.


Example of executing genVHDL with auto validation and sample.xml
as input and sample.vhdl as output files:

genVHDL -o=sample.vhdl sample.xml



REFERENCES:

[1] Shahid Alam; "genVHDL: Automatic HDL code generation from high level XML
    based hardware specifications".
