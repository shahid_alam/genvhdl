#include "symtable.h"

using namespace khudi;

// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
symTable::symTable ()
{
	table[eOR].action  = &genEntityOR;
	table[eAND].action  = &genEntityAND;
	table[eNAND].action = &genEntityNAND;
	table[eXOR].action  = &genEntityXOR;
	table[eMUX].action  = &genEntityMUX;
	table[eCLA].action  = &genEntityCLA;
}

symTable::~symTable ()
{
}

tLinkSymTable symTable::getActionLink (long int nodeIndex)
{
	if(nodeIndex >= 0 && nodeIndex < MAX_NUMBER_NODES)
	{
		return (table[nodeIndex].action);
	}
	else
		return NULL;
}

// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
const void symTable::printSymbol (long int index)
{
	if(index >= 0 && index < MAX_NUMBER_NODES)
		fprintf(stdout, "table[%d] = %s ", index, table[index].action);
}

// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
const void symTable::print (void)
{
	for(int i = 0; i < MAX_NUMBER_NODES; i++)
	{
		fprintf(stdout, "table[%d] = %s ", i, table[i].action);
	}
}
