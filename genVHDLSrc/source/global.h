/* ----------------------------------------------------------------------------
|
|   Filename:    global.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file to be included in all the files
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifndef NULL
	#define NULL 0
#endif

#define MAX_STRING_SIZE_CODE   4096   // 4 KB
#define END_OF_STRING          '\0'
#define MAX_STRING_SIZE        1024   // 1 KB
#define MAX_FILE_NAME          128

#endif // _GLOBAL_H_