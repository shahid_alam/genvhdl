#include "list.h"

using namespace khudi;

// Constructor function.
List :: List ()
{
	firstptr = lastptr = 0;
}

// Destructor function.
List :: ~List ()
{
	if (!isempty()) {
		ListNode *tempptr = NULL;
		ListNode *currentptr = firstptr;
		while (currentptr != lastptr) {
			tempptr = currentptr;
			currentptr = currentptr-> getNextPtr ();
			delete ((void *)tempptr->getDataPtr());
			delete tempptr;
		}
	}
}

// Dynamically allocates memory for the new Node.
ListNode * List :: getNewNode (const char *dPtr)
{
	ListNode *ptr = new ListNode (dPtr);
	return ptr;
}

// Returns first pointer
void * List::getFirst (void)
{
	return firstptr;
}

// Returns next pointer from a current pointer
void * List::getNext (void * cPtr)
{
	ListNode *tempPtr = (ListNode *)cPtr;
	return tempPtr-> getNextPtr ();
}

// Returns data pointer
const char * List::getData (void *nPtr)
{
	ListNode *tempPtr = (ListNode *)nPtr;
	return tempPtr->getDataPtr ();
}

// Prints the list for debugging. CALLBACK the function passed
// to it to let the callee print the data. Because data can have
// different strucure.
void List :: print(void(*fPtr)(const char *)) const
{
	fprintf(stdout, "\n--- Printing Nodes ---\n");

	if (!isempty()) {
		ListNode *nextptr = firstptr;
		fPtr(nextptr->getDataPtr());
		while (nextptr != lastptr) {
			nextptr = nextptr-> getNextPtr ();
			fPtr(nextptr->getDataPtr());
		}
	}
}

// Appends a List. If it is empty new Node will be added
// and the pointer of the Node will be assigned to first else
// it will be assigned to last and the previous last pointer's
// next pointer will be set to the newptr.
void List :: append (const char *dPtr)
{
	ListNode *newptr = getNewNode (dPtr);

   	if (isempty())
		firstptr = newptr;
	else
		lastptr-> setNextPtr (newptr);
	lastptr = newptr;
}

// Removes the last Node of the list. It starts with the
// firstptr then reach to the lastptr keeping track of the
// previous pointer because then that becomes the lastptr
// when lastptr is removed.
void List :: removeLast ()
{
	if (!isempty()) {
		ListNode *nextptr = NULL;
		ListNode *tempptr = firstptr;
		if (firstptr == lastptr)
			firstptr = lastptr = 0;
		while (tempptr != lastptr) {
			nextptr = tempptr-> getNextPtr();
			if (nextptr == lastptr) {
				lastptr = tempptr;
				break;
			}
			tempptr = tempptr-> getNextPtr ();
		}
		delete tempptr;
	}
}

// It counts the number of Nodes in the List by counting
// the linking pointers of the List using getNextPtr ()
// function.
long int List :: count (void) const
{
	long int n(0);

	if (!isempty()) {
		ListNode *tempptr = firstptr;
		while (tempptr != lastptr) {
			n = n + 1;
			tempptr = tempptr-> getNextPtr ();
		}
		n = n + 1;
	}
	return n;
}

// It adds a Node with a value(val) at the given position(posn).
// If the user gives the position as -1 it will however add the
// Node but with a warning. Position '0' is for first Node.
void List :: addN (const char *dPtr, long int posn)
{
	if (posn < 0) {
		fprintf(stderr, "Warning -- Attempt to add before the");
		fprintf(stderr, " beginning of the list.\nAdding as the first");
		fprintf(stderr, " node.\n");
	}
	if (isempty() || posn >= count())
 		append (dPtr);
	else if (posn <= 0) {
		ListNode *tempptr = NULL;
		ListNode *newptr = getNewNode (dPtr);
		newptr-> setNextPtr (firstptr);
		firstptr = newptr;
	}
	else {
		ListNode *tempptr = NULL;
		ListNode *newptr = getNewNode (dPtr);
		ListNode *currentptr = firstptr;
		for (long int n = 1;n < posn;n++)
			currentptr = currentptr-> getNextPtr ();
		tempptr = currentptr-> getNextPtr (); 
		currentptr-> setNextPtr (newptr);
		newptr-> setNextPtr (tempptr);
	}
}
