/* ----------------------------------------------------------------------------
|
|   Filename:    gencode.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file to be included in genCodeVHDL and AST
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _GENCODE_H_
#define _GENCODE_H_

#include "global.h"

/**
*
* Function defined in genCodeVHDL.cpp to be linked in the Symbol / Hash Table
* for creating actions to generate VHDL code for OR gate
*
*/
const void genEntityOR (unsigned int numberOfDataInput, unsigned int notUsed);

/**
*
* Function defined in genCodeVHDL.cpp to be linked in the Symbol / Hash Table
* for creating actions to generate VHDL code for AND gate
*
*/
const void genEntityAND (unsigned int numberOfDataInput, unsigned int notUsed);

/**
*
* Function defined in genCodeVHDL.cpp to be linked in the Symbol / Hash Table
* for creating actions to generate VHDL code for NAND gate
*
*/
const void genEntityNAND (unsigned int numberOfDataInput, unsigned int notUsed);

/**
*
* Function defined in genCodeVHDL.cpp to be linked in the Symbol / Hash Table
* for creating actions to generate VHDL code for XOR gate
*
*/
const void genEntityXOR (unsigned int numberOfDataInput, unsigned int notUsed);

/**
*
* Function defined in genCodeVHDL.cpp to be linked in the Symbol / Hash Table
* for creating actions to generate VHDL code for MULTIPLEXER
*
*/
const void genEntityMUX (unsigned int numberOfDataInput, unsigned int widthOfDataInput);

/**
*
* Function defined in genCodeVHDL.cpp to be linked in the Symbol / Hash Table
* for creating actions to generate VHDL code for CARRY LOOK AHEAD ADDER
*
*/
const void genEntityCLA (unsigned int widthOfDataInput, unsigned int notUsed);

/**
*
* Pointer listed as array in the symbol / hash table.
* Points to functions above defined in genCodeVHDL.cpp
*
*/
typedef const void (*tLinkSymTable) (unsigned int, unsigned int);

/**
*
* Copy 'src' String to 'tgt' String including 'from' and 'to' as starting and ending indices
* @param src source string to be copied from
* @param from position in the src string to start from
* @param to end position in the src string to finish to
* @param tgt target string to be copied to
*
*/
int copyStringToString (const char *src, int from, int to, char *tgt);

#endif // _GENCODE_H_