// ---------------------------------------------------------------------------
//  Includes
// ---------------------------------------------------------------------------
#include "processXML.h"

processXML::processXML (char *fileNameVHDL)
{
	currentNode = -1;
	astHandler = new AST ();
	genVHDLhandler = new genVHDL (fileNameVHDL);
}

processXML::~processXML ()
{
	delete (astHandler);
	delete (genVHDLhandler);
}

// ---------------------------------------------------------------------------
//  SAX2PrintHandlers: Overrides of the SAX ErrorHandler interface
// ---------------------------------------------------------------------------
// Handle notification of a recoverable parser error.
void processXML::error (const SAXParseException& e)
{
    fprintf(stderr, "\nError at file %s line %d, char %d\n  Message: %s\n",
                                     XMLString::transcode (e.getSystemId()),
                                     e.getLineNumber (),
                                     e.getColumnNumber (),
                                     XMLString::transcode (e.getMessage ()));
}

// ---------------------------------------------------------------------------
// Handle a fatal XML parsing error
// ---------------------------------------------------------------------------
void processXML::fatalError (const SAXParseException& e)
{
    fprintf(stderr, "\nFatal Error at file %s line %d, char %d\n  Message: %s\n",
                                     XMLString::transcode(e.getSystemId ()),
                                     e.getLineNumber (),
                                     e.getColumnNumber (),
                                     XMLString::transcode (e.getMessage ()));
}

// ---------------------------------------------------------------------------
// Handle notification of a parser warning
// ---------------------------------------------------------------------------
void processXML::warning (const SAXParseException& e)
{
    fprintf(stderr, "\nWarning at file %s line %d, char %d\n  Message: %s\n",
                                     XMLString::transcode(e.getSystemId ()),
                                     e.getLineNumber (),
                                     e.getColumnNumber (),
                                     XMLString::transcode (e.getMessage ()));
}

// ---------------------------------------------------------------------------
// Handle notification of character data inside an element.
// ---------------------------------------------------------------------------
void processXML::characters (const XMLCh* const chars,
                                 const unsigned int length)
{
    memmove(text, XMLString::transcode(chars), length);
	text[length] = END_OF_STRING;

#ifdef _DEBUG
	printf ("%s", text);
#endif
}

// ---------------------------------------------------------------------------
// Handle notification of the end of the document
// ---------------------------------------------------------------------------
void processXML::endDocument ()
{
#ifdef _DEBUG
	astHandler->print ();
#endif

	genVHDLhandler->processAST (astHandler);
}

// ---------------------------------------------------------------------------
// Handle notification of the end of an element
// ---------------------------------------------------------------------------
void processXML::endElement (const XMLCh* const uri, const XMLCh* const localname,
							                                 const XMLCh* const qname)
{
	strcpy (element, XMLString::transcode (qname));

	if (currentNode == eOR)
	{
		if (strncmp (element, cTagName[eOR], strlen (cTagName[eOR])) == 0)
		{
			astHandler->insertOR (or.numberOfDataInput);
			currentNode = -1;
		}
		else if(strncmp (element, "numberOfDataInput", 17) == 0)
			or.numberOfDataInput = atoi (text);
	}
	else if (currentNode == eAND)
	{
		if (strncmp (element, cTagName[eAND], strlen (cTagName[eAND])) == 0)
		{
			astHandler->insertAND (and.numberOfDataInput);
			currentNode = -1;
		}
		else if (strncmp (element, "numberOfDataInput", 17) == 0)
			and.numberOfDataInput = atoi (text);
	}
	else if (currentNode == eNAND)
	{
		if (strncmp (element, cTagName[eNAND], strlen (cTagName[eNAND])) == 0)
		{
			astHandler->insertNAND (nand.numberOfDataInput);
			currentNode = -1;
		}
		else if (strncmp (element, "numberOfDataInput", 17) == 0)
			nand.numberOfDataInput = atoi (text);
	}
	else if (currentNode == eXOR)
	{
		if (strncmp(element, cTagName[eXOR], strlen (cTagName[eXOR])) == 0)
		{
			astHandler->insertXOR (xor.numberOfDataInput);
			currentNode = -1;
		}
		else if (strncmp (element, "numberOfDataInput", 17) == 0)
			xor.numberOfDataInput = atoi (text);
	}
	else if (currentNode == eMUX)
	{
		if (strncmp (element, cTagName[eMUX], strlen (cTagName[eMUX])) == 0)
		{
			astHandler->insertMUX (mux.numberOfDataInput, mux.widthOfDataInput);
			currentNode = -1;
		}
		else if (strncmp (element, "numberOfDataInput", 17) == 0)
			mux.numberOfDataInput = atoi (text);
		else if (strncmp (element, "widthOfDataInput", 16) == 0)
			mux.widthOfDataInput = atoi (text);
	}
	else if (currentNode == eCLA)
	{
		if (strncmp(element, cTagName[eCLA], strlen (cTagName[eCLA])) == 0)
		{
			astHandler->insertCLA (cla.widthOfDataInput);
			currentNode = -1;
		}
		else if (strncmp (element, "widthOfDataInput", 16) == 0)
			cla.widthOfDataInput = atoi (text);
	}

#ifdef _DEBUG
	printf ("</%s>\n", element);
#endif
}

// ---------------------------------------------------------------------------
// Handle notification of ignorable whitespace in element content
// ---------------------------------------------------------------------------
void processXML::ignorableWhitespace (const XMLCh* const chars,
                                          const unsigned int length)
{
    memmove(element, XMLString::transcode (chars), length);
	element[length] = '\0';

#ifdef _DEBUG
	printf ("%s", element);
#endif
}


// ---------------------------------------------------------------------------
// Handle notification of the beginning of the document
// ---------------------------------------------------------------------------
void processXML::startDocument ()
{
}

// ---------------------------------------------------------------------------
// Handle notification of the start of an element. 
// ---------------------------------------------------------------------------
void processXML::startElement (const XMLCh* const uri, const XMLCh* const localname,
                                   const XMLCh* const qname, const Attributes& attributes)
{
	strcpy(element, XMLString::transcode (qname));

	if(strncmp (element, cTagName[eOR], strlen (cTagName[eOR])) == 0)
		currentNode = eOR;
	else if (strncmp (element, cTagName[eAND], strlen (cTagName[eAND])) == 0)
		currentNode = eAND;
	else if (strncmp (element, cTagName[eNAND], strlen (cTagName[eNAND])) == 0)
		currentNode = eNAND;
	else if (strncmp (element, cTagName[eXOR], strlen (cTagName[eXOR])) == 0)
		currentNode = eXOR;
	else if (strncmp (element, cTagName[eMUX], strlen (cTagName[eMUX])) == 0)
		currentNode = eMUX;
	else if (strncmp (element, cTagName[eCLA], strlen (cTagName[eCLA])) == 0)
		currentNode = eCLA;

#ifdef _DEBUG
	printf ("<%s>", element);
#endif
}
