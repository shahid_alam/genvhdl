/* ----------------------------------------------------------------------------
|
|   Filename:    gencode.cpp
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Contains utility functions to be used in each of the subsequent
|                genCode**** files.
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#include "gencode.h"

/* ==========================================================================

	init() function

PURPOSE:	Copy 'src' String to 'tgt' String including from and to
            as starting and ending indices
SETUP:		None.
PARAMETERS: src: Source string to be copied from
            from: Position in the src string to start from
			to: End position in the src string to finish to
			tgt: Target string to be copied to
CALL:		init();
RETURN:		None.

---------------------------------------------------------------------------*/
int copyStringToString (const char *src, int from, int to, char *tgt)
{
	if (to > from)
	{
		int size = to - from;
		int s1 = strlen (src);
		int s2 = strlen (tgt);
		if (s1 >= size && s2 >= size)
		{
			for (int i = 0; i < size; i++)
			{
				tgt[i] = src[i+from];
			}

			tgt[size] = END_OF_STRING;
			return size;
		}
	}

	return 0;
}
