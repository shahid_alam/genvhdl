// ---------------------------------------------------------------------------
//  Includes
// ---------------------------------------------------------------------------
#include "genVHDL.h"

using namespace khudi;

// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
genVHDL::genVHDL ()
{
}

// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
genVHDL::genVHDL (const char* fileName)
{
	stringBufSize = NULL;
	init();

	// Open for write
	if ((filePointer  = fopen (fileName, "wt")) == NULL)
		fprintf (stderr, "The file %s was not opened\n", fileName);
}

// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
genVHDL::~genVHDL ()
{
	clean ();
	fclose (filePointer);
	fcloseall ();
}

// ---------------------------------------------------------------------------
//
// Call the appropiate action function from the symbol table to generate code
//
// ---------------------------------------------------------------------------
void genVHDL::processAST (AST *astHandler)
{
	unsigned int node = -1;
	AST *ast = astHandler;
	const char * astNodePtr;

	astNodePtr = ast->getFirstNode ();

	while (astNodePtr != NULL)
	{
		node = (unsigned int)(*astNodePtr);

		switch (node)
		{
		case eOR:
			((OR *)astNodePtr)->linkSymTable (((OR *)astNodePtr)->numberOfDataInput, -1);
			writeFile ();
			break;
		case eAND:
			((AND *)astNodePtr)->linkSymTable (((AND *)astNodePtr)->numberOfDataInput, -1);
			writeFile ();
			break;
		case eNAND:
			((NAND *)astNodePtr)->linkSymTable (((NAND *)astNodePtr)->numberOfDataInput, -1);
			writeFile ();
			break;
		case eXOR:
			((XOR *)astNodePtr)->linkSymTable (((XOR *)astNodePtr)->numberOfDataInput, -1);
			writeFile ();
			break;
		case eMUX:
			((MUX *)astNodePtr)->linkSymTable (((MUX *)astNodePtr)->numberOfDataInput, ((MUX *)astNodePtr)->widthOfDataInput);
			writeFile ();
			break;
		case eCLA:
			((CLA *)astNodePtr)->linkSymTable (((CLA *)astNodePtr)->widthOfDataInput, -1);
			writeFile ();
			break;
		default:
			// do nothing
			break;
		}

		astNodePtr = ast->getNextNode ();
	}
}

// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
void genVHDL::writeFile ()
{
	int size = stringBufCode.size;

	if(size <= MAX_STRING_SIZE_CODE)
	{
		stringBufCode.string[size] = END_OF_STRING;
#ifdef _DEBUG
		printf("\n--- Printing the Current Code Buffer ---\n%s\n", stringBufCode.string);
#endif
		fwrite((const char *)stringBufCode.string, size, 1, filePointer);
		fflush(filePointer);
		// cleaning the code buffer
		clean();
	}
	else
		fprintf(stderr, "genVHDL::writeFile (): Size of the String too big\n");
}
