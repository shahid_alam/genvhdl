/* ----------------------------------------------------------------------------
|
|   Filename:    symtable.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for symtable.cpp
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _SYMTABLE_H_
#define _SYMTABLE_H_

#include "global.h"
#include "gencode.h"
#include "xmlstruct.h"

namespace khudi
{
	/**
	*
	* It's a simple hash table for storing all the Nodes defined in XML Schema. 
	* Symbol Table is a static list of components with a link to the 
	* action function. Since we already know the number of components 
	* so it was decided to keep this static. Advantage is that searching 
	* is O(1) complex. It also doesn't take as much space as the number of 
	* components. Currently only one action is added that creates the structure 
	* and behavior of the digital component, which is already defined. So it 
	* doesn't give flexibility to the user to change the behavior, in the XML. 
	* Latter this facility can be added in the form of an Action Language, 
	* based on Action Semantics.
	*
	*/
	class symTable
	{
	public:
		/**
		*
		* Constructor. It creates all the links to the action functions 
		* defined in gencode.h
		*
		*/
		symTable ();
		/**
		*
		* Destructor
		*
		*/
		~symTable ();
		/**
		*
		* Searches the link to the action function for particular Node 
		* from the table in constant O(1) time.
		* @param nodeIndex index / hash of the Node stored in the table.
		*
		*/
		tLinkSymTable getActionLink (long int nodeIndex);
		/**
		*
		* Prints the action function for particular Node for debugging
		* @param index index / hash of the Node stored in the table.
		*
		*/
		const void printSymbol (long int index);
		/**
		*
		* Prints the complete symbol table for debugging
		*
		*/
		const void print (void);

	private:
		/**
		*
		* Declaration of the hash table for storing all the Nodes 
		* defined in XML Schema with a link to the action function 
		* for that particular Node.
		*
		*/
		typedef struct stable
		{
			tLinkSymTable action;
		} Table;
		Table table[MAX_NUMBER_NODES];
	};
}

#endif // _SYMTABLE_H_