/* ----------------------------------------------------------------------------
|
|   Filename:    list.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header for Link List class in list.cpp
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _LIST_H_
#define _LIST_H_

#include "global.h"

namespace khudi
{
	/**
	*
	* Class ListNode. Data structure for a Node in the List.
	*
	*/
	class ListNode
	{
		/**
		*
		* Data Pointer points to the data stored in the list. It can be 
		* be any data structure. But then handling of data structure is 
		* the responsibility of the caller.
		*
		*/
		const char *dataPtr;
		/**
		*
		* Next and Previous pointers
		*
		*/
		ListNode *nextptr, *prevptr;
	public:						
		/**
		*
		* Constructor
		* @param dPtr data pointer points to the data stored in the Node
		* @see dataPtr
		*
		*/
		ListNode (const char *dPtr)
		{
			dataPtr = dPtr;
			nextptr = 0;
		};

		/**
		*
		* Sets the pointer to the next Node.
		* @param ptr pointer to the Node in the List
		* @see ListNode
		*
		*/
		void setNextPtr (ListNode *ptr) { 
			nextptr = ptr;
			return;
		};

		/**
		*
		* Gets the next pointer of the Node
		* @return pointer to ListNode
		* @see ListNode
		*
		*/
		ListNode* getNextPtr () const
		{
			return nextptr;
		};

		/**
		*
		* Sets the previous pointer of the Node.
		* @param ptr pointer to the ListNode
		* @see ListNode
		*
		*/
		void setPrevPtr (ListNode *ptr) {
			prevptr = ptr;
			return;
		};

		/**
		*
		* Gets the previous pointer of the Node.
		* @return pointer to ListNode
		* @see ListNode
		*
		*/
		ListNode* getPrevPtr () const
		{
			return prevptr;
		};

		/**
		*
		* Gets the value of the data.
		* @return pointer to data linked in the List
		* @see dataPtr
		*
		*/
		const char * getDataPtr () const
		{
			return dataPtr;
		};
	};

	/**
	*
	* Class List. Implements a linked list
	*
	*/
	class List
	{
		/**
		*
		* pointers for first and last Node in the List.
		* @see ListNode
		*
		*/
		ListNode *firstptr, *lastptr;
		/**
		*
		* Dynamically allocates memory for the new Node.
		* @param dPtr data pointer points to the data stored in the Node
		* @see dataPtr
		*
		*/
		ListNode *getNewNode (const char *dPtr);
	public:
		/**
		*
		* Constructor
		*
		*/
		List ();
		/**
		*
		* Destructor
		*
		*/
		~List ();
		/**
		*
		*
		*/
		const char * getData (void *nPtr);
		/**
		*
		* Returns first pointer
		* @return void pointer
		*/
		void * getFirst (void);
		/**
		*
		* Returns next pointer
		* @param cPtr current pointer to the Node
		*
		*/
		void * getNext (void * cPtr);
		/**
		*
		* Appends a List. If it is empty new Node will be added 
		* and the pointer of the Node will be assigned to first else 
		* it will be assigned to last and the previous last pointer's 
		* next pointer will be set to the newptr.
		* @param dPtr data pointer points to the data stored in the Node
		* @see ListNode::dataPtr
		*
		*/
		void append (const char *dPtr);
		/**
		*
		* Removes the last Node of the list. It starts with the 
		* firstptr then reach to the lastptr keeping track of the 
		* previous pointer because then that becomes the lastptr 
		* when lastptr is removed.
		*
		*/
		void removeLast (void);
		/**
		*
		* It counts the number of Nodes in the List by counting 
		* the linking pointers of the List using getNextPtr () 
		* function.
		* @return the number of nodes in the List
		*
		*/
		long int count (void) const;
		/**
		*
		* It adds a Node with a value(val) at the given position(posn). 
		* If the user gives the position as -1 it will however add the 
		* Node but with a warning. Position '0' is for first Node.
		* @param dPtr data pointer points to the data stored in the Node
		* @see ListNode::dataPtr
		* @param posn position in the List where a new Node will added
		*
		*/
		void addN (const char *dPtr, long int posn);
		/**
		*
		* Prints the List for debugging. CALLBACK the function passed
		* to it to let the caller print the data. Because data can have
		* different strucure. And it should be handled by the caller.
		* @see ListNode::dataPtr
		* @param fPtr function pointer passed to be called while 
		* traversing the list. Function has one input parameter which 
		* points to the data stored in the List.
		*
		*/
		void print (void(*fPtr)(const char *)) const;

		/**
		*
		* Check if List is empty or not
		* @return 0 if List is empty
		*
		*/
		int isempty (void) const
		{
			return firstptr == 0;
		};
	};

}

#endif // _LIST_H_