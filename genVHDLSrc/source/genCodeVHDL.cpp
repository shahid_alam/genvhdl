/* ----------------------------------------------------------------------------
|
|   Filename:    genCodeVHDL.cpp
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: To generate VHDL code. Link to these functions are provided
|                in the AST node. Currently both structure and behavior part
|                is generated in the same function. The rule used to generate
|                the code is general. It's not specific to any action etc.
|
|   TO DO:       Actions needs to be defined for the beahvior part. So that
|                the behavoir can be controlled by the action defined in the
|                form of an action language.
|
 ----------------------------------------------------------------------------*/

#include "genCodeVHDL.h"

/* ============================================================================

Local data

-----------------------------------------------------------------------------*/
long int countNode[MAX_NUMBER_NODES];
tStringBufCode stringBufCode;
char tempStringBuf[MAX_STRING_SIZE];

/* ==========================================================================

	init () function

PURPOSE:	This function initializes the program.
SETUP:		None.
PARAMETERS: None.
CALL:		init ();
RETURN:		None.

---------------------------------------------------------------------------*/
void init (void)
{
	for(int i = 0; i < MAX_NUMBER_NODES; i++)
		countNode[i] = 0;
	clean();
}

/* ==========================================================================

	copyString () function

PURPOSE:	Concatenate string to the global string for writing to the file
            It also increments the total size of the string
SETUP:		None.
PARAMETERS: stringBuf: String to be copied
            size: Size of the stringBuf to be copied
CALL:		copyString (char *, int);
RETURN:		None.

---------------------------------------------------------------------------*/
void copyString (const char *stringBuf, int size)
{
	if (size > MAX_STRING_SIZE)
	{
		fprintf (stderr, "genCodeVHDL::copyString: String too big\n");
	}
	else
	{
		strncat (stringBufCode.string, stringBuf, size);
		stringBufCode.size = size + stringBufCode.size;
	}
}

/* ==========================================================================

	copyComment () function

PURPOSE:	Copy comments to the gloabl string code buffer. Comment
            shouldn't have end of line. This function takes care of that.
SETUP:		None.
PARAMETERS: comment: Comment string to be copied
            size: Size of the comment string
CALL:		copyComment(const char *, int);
RETURN:		None.

---------------------------------------------------------------------------*/
void copyComment (const char *comment, int size)
{
	if (size > MAX_STRING_SIZE)
	{
		fprintf (stderr, "genCodeVHDL::copyComment: Comment too big\n");
		return;
	}

	int i;

	// Printing the upper line
	copyString ("\n", 1);
	for (i = 0; i < (COMMENT_LINE_SIZE+6)/2; i++)
	{
		copyString ("--", 2);
	}
	copyString ("\n--\n", 4);

	if (size > COMMENT_LINE_SIZE)
	{
		int rSize;
		char sBuf[MAX_STRING_SIZE];

		for (i = 0; i < (size - COMMENT_LINE_SIZE); i += COMMENT_LINE_SIZE)
		{
			copyString ("-- ", 3);
			rSize = copyStringToString (comment, i, (i+COMMENT_LINE_SIZE), sBuf);
			copyString (sBuf, rSize);
			copyString ("\n", 1);
		}
		rSize = size % COMMENT_LINE_SIZE;
		if (rSize > 0)
		{
			copyString ("-- ", 3);
			rSize = copyStringToString (comment, i, size, sBuf);
			copyString (sBuf, rSize);
			copyString ("\n", 1);
		}
	}
	else
	{
		copyString ("-- ", 3);
		copyString ((char *)comment, strlen(comment));
	}

	// Printing the lower line
	copyString ("--\n", 3);
	for (i = 0; i < (COMMENT_LINE_SIZE+6)/2; i++)
	{
		copyString ("--", 2);
	}
	copyString ("\n", 1);
}

/* ==========================================================================

	copyVHDLheader () function

PURPOSE:	Copy the VHDL header to string code buffer
SETUP:		None.
PARAMETERS: None.
CALL:		copyVHDLheader ();
RETURN:		None.

---------------------------------------------------------------------------*/
void copyVHDLheader (void)
{
	copyString ((char *)VHDLheader, strlen(VHDLheader));
}

/* ==========================================================================

	genEntityOR () function

PURPOSE:	It generates code for the structure of the AND library component
            Currently it's also generating code for the behavior of the AND
            Latter that will be moved to another function after the
			introduction of the Action Language.
SETUP:		None.
PARAMETERS: numberOfDataInput: Number of data input to the multiplexer
            notUsed: Currently not used
CALL:		genEntityAND (unsigned int, unsigned int);
RETURN:		None.

---------------------------------------------------------------------------*/
const void genEntityOR (unsigned int numberOfDataInput, unsigned int notUsed)
{
	if(numberOfDataInput <= 0)
		return;

	unsigned int i = 0;
	long int size = 0;
	char orStr[MAX_XML_ELEMENT_SIZE];

	countNode[eOR]++;
	size = sprintf (orStr, "OR_%d", countNode[eOR]);
	orStr[size] = END_OF_STRING;

/*	size = sprintf (tempStringBuf, "Implementation of AND gate # %d "
									"Which is bla bla bla "
									"testing for comments, if they can pass "
									"the test of line whihc is greater thatn 60 words."
									"But what if you find that there are certain things"
									"those can't be solved by the employee"
									, countNode[eAND]);
	copyComment (tempStringBuf, size);*/

	copyVHDLheader ();

	size = sprintf (tempStringBuf, "entity %s is\n", orStr);
	copyString (tempStringBuf, size);
	size = sprintf (tempStringBuf, "   port(");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d: in std_ulogic;\n"
									"        z: out std_ulogic\n"
									"   );\n"
									"end %s;\n\n", i, orStr);
	copyString (tempStringBuf, size);

	//
	// STARTING BEHAVIOR CODING
	//
	size = sprintf (tempStringBuf, "architecture %sa of %s is\n"
									"begin\n"
									"   or_process: process(", orStr, orStr);
	copyString (tempStringBuf, size);
	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d) is\n", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, "   begin\n"
									"      z <= ");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-2; i++)
		copyString ("(", 1);
	size = sprintf (tempStringBuf, "d0");
	copyString (tempStringBuf, size);
	for(i = 1; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, " or d%d)", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, " or d%d", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, 	";\n   end process or_process;\n"
									"end architecture %sa;\n\n\n"
									, orStr);
	copyString (tempStringBuf, size);
}

/* ==========================================================================

	genEntityAND () function

PURPOSE:	It generates code for the structure of the AND library component
            Currently it's also generating code for the behavior of the AND
            Latter that will be moved to another function after the
			introduction of the Action Language.
SETUP:		None.
PARAMETERS: numberOfDataInput: Number of data input to the multiplexer
            notUsed: Currently not used
CALL:		genEntityAND (unsigned int, unsigned int);
RETURN:		None.

---------------------------------------------------------------------------*/
const void genEntityAND (unsigned int numberOfDataInput, unsigned int notUsed)
{
	if(numberOfDataInput <= 0)
		return;

	unsigned int i = 0;
	long int size = 0;
	char andStr[MAX_XML_ELEMENT_SIZE];

	countNode[eAND]++;
	size = sprintf (andStr, "AND_%d", countNode[eAND]);
	andStr[size] = END_OF_STRING;

	copyVHDLheader ();

	size = sprintf (tempStringBuf, "entity %s is\n", andStr);
	copyString (tempStringBuf, size);
	size = sprintf (tempStringBuf, "   port(");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d: in std_ulogic;\n"
									"        z: out std_ulogic\n"
									"   );\n"
									"end %s;\n\n", i, andStr);
	copyString (tempStringBuf, size);

	//
	// STARTING BEHAVIOR CODING
	//
	size = sprintf (tempStringBuf, "architecture %sa of %s is\n"
									"begin\n"
									"   and_process: process(", andStr, andStr);
	copyString (tempStringBuf, size);
	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d) is\n", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, "   begin\n"
									"      z <= ");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-2; i++)
		copyString ("(", 1);
	size = sprintf (tempStringBuf, "d0");
	copyString (tempStringBuf, size);
	for(i = 1; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, " and d%d)", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, " and d%d", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, 	";\n   end process and_process;\n"
									"end architecture %sa;\n\n\n"
									, andStr);
	copyString (tempStringBuf, size);
}

/* ==========================================================================

	genEntityNAND () function

PURPOSE:	It generates code for the structure of the NAND library component
            Currently it's also generating code for the behavior of the NAND
            Latter that will be moved to another function after the
			introduction of the Action Language.
SETUP:		None.
PARAMETERS: numberOfDataInput: Number of data input to the multiplexer
            notUsed: Currently not used
CALL:		genEntityNAND (unsigned int, unsigned int);
RETURN:		None.

---------------------------------------------------------------------------*/
const void genEntityNAND (unsigned int numberOfDataInput, unsigned int notUsed)
{
	if(numberOfDataInput <= 0)
		return;

	unsigned int i = 0;
	long int size = 0;
	char nandStr[MAX_XML_ELEMENT_SIZE];

	countNode[eNAND]++;
	size = sprintf (nandStr, "NAND_%d", countNode[eNAND]);
	nandStr[size] = END_OF_STRING;

	copyVHDLheader ();

	size = sprintf (tempStringBuf, "entity %s is\n", nandStr);
	copyString (tempStringBuf, size);
	size = sprintf (tempStringBuf, "   port(");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d: in std_ulogic;\n"
									"        z: out std_ulogic\n"
									"   );\n"
									"end %s;\n\n", i, nandStr);
	copyString (tempStringBuf, size);

	//
	// STARTING BEHAVIOR CODING
	//
	size = sprintf (tempStringBuf, "architecture %sa of %s is\n"
									"begin\n"
									"   nand_process: process(", nandStr, nandStr);
	copyString (tempStringBuf, size);
	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d) is\n", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, "   begin\n"
									"      z <= ");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-2; i++)
		copyString ("(", 1);
	size = sprintf (tempStringBuf, "d0");
	copyString (tempStringBuf, size);
	for(i = 1; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, " nand d%d)", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, " nand d%d", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, 	";\n   end process nand_process;\n"
									"end architecture %sa;\n\n\n"
									, nandStr);
	copyString (tempStringBuf, size);
}

/* ==========================================================================

	genEntityXOR () function

PURPOSE:	It generates code for the structure of the XOR library component
            Currently it's also generating code for the behavior of the XOR
            Latter that will be moved to another function after the
			introduction of the Action Language.
SETUP:		None.
PARAMETERS: numberOfDataInput: Number of data input to the multiplexer
            notUsed: Currently not used
CALL:		genEntityXOR (unsigned int, unsigned int);
RETURN:		None.

---------------------------------------------------------------------------*/
const void genEntityXOR (unsigned int numberOfDataInput, unsigned int notUsed)
{
	if(numberOfDataInput <= 0)
		return;

	unsigned int i = 0;
	long int size = 0;
	char xorStr[MAX_XML_ELEMENT_SIZE];

	countNode[eXOR]++;
	size = sprintf (xorStr, "XOR_%d", countNode[eXOR]);
	xorStr[size] = END_OF_STRING;

	copyVHDLheader ();

	size = sprintf (tempStringBuf, "entity %s is\n", xorStr);
	copyString (tempStringBuf, size);
	size = sprintf (tempStringBuf, "   port(");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d: in std_ulogic;\n"
									"        z: out std_ulogic\n"
									"   );\n"
									"end %s;\n\n", i, xorStr);
	copyString (tempStringBuf, size);

	//
	// STARTING BEHAVIOR CODING
	//
	size = sprintf (tempStringBuf, "architecture %sa of %s is\n"
									"begin\n"
									"   xor_process: process(", xorStr, xorStr);
	copyString (tempStringBuf, size);
	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d) is\n", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, "   begin\n"
									"      z <= ");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput-2; i++)
		copyString ("(", 1);
	size = sprintf (tempStringBuf, "d0");
	copyString (tempStringBuf, size);
	for(i = 1; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, " xor d%d)", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, " xor d%d", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, 	";\n   end process xor_process;\n"
									"end architecture %sa;\n\n\n"
									, xorStr);
	copyString (tempStringBuf, size);
}

/* ==========================================================================

	genEntityMUX () function

PURPOSE:	It generates code for the structure of the MUX library component
            Currently it's also generating code for the behavior of the MUX
            Latter that will be moved to another function after the
			introduction of the Action Language.
SETUP:		None.
PARAMETERS: numberOfDataInput: Number of data input to the multiplexer
            widthOfDataInput: Width of the data input (1, 8, 16, 32, 64 bits)
CALL:		genEntityMUX (unsigned int, unsigned int);
RETURN:		None.

---------------------------------------------------------------------------*/
const void genEntityMUX (unsigned int numberOfDataInput, unsigned int widthOfDataInput)
{
	if(numberOfDataInput <= 0 || widthOfDataInput <= 0)
		return;

	unsigned int i = 0;
	long int size = 0;
	char muxStr[MAX_XML_ELEMENT_SIZE];

	countNode[eMUX]++;
	size = sprintf (muxStr, "MUX_%d", countNode[eMUX]);
	muxStr[size] = END_OF_STRING;

	copyVHDLheader ();

	size = sprintf (tempStringBuf, "entity %s is\n", muxStr);
	copyString (tempStringBuf, size);
	size = sprintf (tempStringBuf, "   port(sel: in integer range 0 to %d;\n", numberOfDataInput-1);
	copyString (tempStringBuf, size);

	size = sprintf(tempStringBuf, "        ");
	copyString (tempStringBuf, size);
	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, "d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, "d%d", i);
	copyString (tempStringBuf, size);

	size = sprintf(tempStringBuf, ": in std_ulogic_vector(%d downto 0);\n"
								"        z: out std_ulogic_vector(%d downto 0)\n"
								"   );\n", widthOfDataInput-1, widthOfDataInput-1);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, "end %s;\n\n", muxStr);
	copyString(tempStringBuf, size);

	//
	// STARTING BEHAVIOR CODING
	//
	size = sprintf (tempStringBuf, "architecture %sa of %s is\n"
								"begin\n"
								"   mux_process: process(sel,", muxStr, muxStr);
	copyString(tempStringBuf, size);
	for(i = 0; i < numberOfDataInput-1; i++)
	{
		size = sprintf (tempStringBuf, " d%d, ", i);
		copyString (tempStringBuf, size);
	}
	size = sprintf (tempStringBuf, " d%d) is\n", i);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, "   begin\n"
								"      case sel is\n");
	copyString (tempStringBuf, size);

	for(i = 0; i < numberOfDataInput; i++)
	{
		size = sprintf (tempStringBuf, "         when %d =>\n", i);
		copyString (tempStringBuf, size);
		size = sprintf (tempStringBuf, "            z <= d%d;\n", i);
		copyString (tempStringBuf, size);
	}

	size = sprintf (tempStringBuf, 	"      end case;\n"
									"   end process mux_process;\n"
									"end architecture %sa;\n\n\n"
									, muxStr);
	copyString (tempStringBuf, size);
}

/* ==========================================================================

	genEntityCLA () function

PURPOSE:	It generates code for the structure of the CLA library component
            Currently it's also generating code for the behavior of the CLA
            Latter that will be moved to another function after the
			introduction of the Action Language.
SETUP:		None.
PARAMETERS: widthOfDataInput: Width of the data input (1, 8, 16, 32, 64 bits)
            notUsed: Currently not used
CALL:		genEntityCLA (unsigned int, unsigned int);
RETURN:		None.

---------------------------------------------------------------------------*/
const void genEntityCLA (unsigned int widthOfDataInput, unsigned int notUsed)
{
	if(widthOfDataInput <= 0)
		return;

	unsigned int i = 0;
	long int size = 0;
	char claStr[MAX_XML_ELEMENT_SIZE];
	char temp[MAX_STRING_SIZE];
	char temp1[MAX_STRING_SIZE];

	countNode[eCLA]++;
	size = sprintf (claStr, "CLA_%d", countNode[eCLA]);
	claStr[size] = END_OF_STRING;

	copyVHDLheader ();

	sprintf (temp, "std_ulogic_vector(%d downto 0);", widthOfDataInput-1);
	sprintf (temp1, "std_ulogic_vector(%d downto 1);", widthOfDataInput-1);

	size = sprintf (tempStringBuf, "ENTITY %s is\n", claStr);
	copyString (tempStringBuf, size);
	size = sprintf (tempStringBuf, "   PORT(x_in        : in %s\n"
									"        y_in        : in %s\n"
									"        carry_in    : in std_ulogic;\n"
									"        sum         : out %s\n"
									"        carry_out   : out std_ulogic\n"
									"   );\n", temp, temp, temp);
	copyString (tempStringBuf, size);
	size = sprintf (tempStringBuf, "END %s;\n\n", claStr);
	copyString (tempStringBuf, size);

	//
	// STARTING BEHAVIOR CODING
	//
	size = sprintf (tempStringBuf, "ARCHITECTURE %sa of %s is\n\n"
								"signal h_sum               : %s\n"
								"signal carry_generate      : %s\n"
								"signal carry_propagate     : %s\n"
								"signal carry_in_internal   : %s\n\n"
								"BEGIN\n", claStr, claStr, temp, temp, temp, temp1);
	copyString(tempStringBuf, size);

	size = sprintf (tempStringBuf, "   h_sum <= x_in XOR y_in;\n"
									"   carry_generate <= x_in AND y_in;\n"
									"   carry_propagate <= x_in OR y_in;\n\n");
	copyString(tempStringBuf, size);



	size = sprintf (tempStringBuf, "   PROCESS (carry_in, carry_generate, carry_propagate, "
													"carry_in_internal)\n"
									"   BEGIN\n"
									"      carry_in_internal(1) <= carry_generate(0) "
													"OR (carry_propagate(0) AND carry_in);\n"
									"         inst: FOR i IN 1 TO %d LOOP\n"
									"            carry_in_internal(i+1) <= carry_generate(i) OR "
													"(carry_propagate(i) AND carry_in_internal(i));\n"
									"         END LOOP;\n", widthOfDataInput-2);
	copyString (tempStringBuf, size);

	size = sprintf (tempStringBuf, "   carry_out <= carry_generate(%d) OR (carry_propagate(%d) "
													"AND carry_in_internal(%d));\n"
									"   END PROCESS;\n\n"
									"   sum(0) <= h_sum(0) XOR carry_in;\n"
									"   sum(%d DOWNTO 1) <= h_sum(%d DOWNTO 1) XOR "
													"carry_in_internal(%d DOWNTO 1);\n"
									"END architecture %sa;\n\n\n"
									, widthOfDataInput-1, widthOfDataInput-1, widthOfDataInput-1,
									widthOfDataInput-1, widthOfDataInput-1, widthOfDataInput-1, claStr);
	copyString (tempStringBuf, size);
}

/* ==========================================================================

	clean () function

PURPOSE:	Cleaning the code buffer
SETUP:		None.
PARAMETERS: None.
CALL:		clean ();
RETURN:		None.

---------------------------------------------------------------------------*/
void clean (void)
{
	stringBufCode.string[0] = END_OF_STRING;
	stringBufCode.size = 0;
}
