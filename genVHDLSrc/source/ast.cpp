/* ----------------------------------------------------------------------------
|
|   Filename:    ast.cpp
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: This is the intermediate representation during translation
|                from XML to the specified language in the form of an
|                Abstract Syntax Tree (AST). AST is a linked list of XML
|                structure. It contains a copy of XML with annotation i.e a
|                link to a symbol table for actions to create code for
|                specified component in a particular language.
|                If traversed from bottom to top, and using the symbol table,
|                it produces the source code for the specified
|                language (VHDL / Verilog).
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#include "ast.h"

using namespace khudi;

/* ============================================================================

Local data

   Pointer to a function 'printList', to be passed as CALLBACK
   function pointer, to List.

-----------------------------------------------------------------------------*/
void printList(const char *);
void (*printListFunc) (const char *) = &printList;

AST :: AST()
{
	symT = new symTable();
}

AST :: ~AST()
{
	delete (symT);
}

// CALLBACK function, to be passed to the print function of class List for
// CALLBACK. It prints the data stored in the list for debugging
void printList(const char *sPtr)
{
	if(sPtr != NULL)
	{
		unsigned int node = (unsigned int)(*sPtr);

		switch (node)
		{
		case eOR:
			fprintf(stdout, "<%s>\n", cTagName[eOR]);
			fprintf(stdout, "   <numberOfDataInput: %d>\n", ((OR *)sPtr)->numberOfDataInput);
			fprintf(stdout, "   <linkSymTable: %d>\n", ((OR *)sPtr)->linkSymTable);
			fprintf(stdout, "   <ACTION>\n");
			// ((OR *)sPtr)->linkSymTable(((OR *)sPtr)->numberOfDataInput, -1);
			fprintf(stdout, "   </ACTION>\n");
			fprintf(stdout, "</%s>\n", cTagName[eOR]);
			break;
		case eAND:
			fprintf(stdout, "<%s>\n", cTagName[eAND]);
			fprintf(stdout, "   <numberOfDataInput: %d>\n", ((AND *)sPtr)->numberOfDataInput);
			fprintf(stdout, "   <linkSymTable: %d>\n", ((AND *)sPtr)->linkSymTable);
			fprintf(stdout, "   <ACTION>\n");
			// ((AND *)sPtr)->linkSymTable(((AND *)sPtr)->numberOfDataInput, -1);
			fprintf(stdout, "   </ACTION>\n");
			fprintf(stdout, "</%s>\n", cTagName[eAND]);
			break;
		case eNAND:
			fprintf(stdout, "<%s>\n", cTagName[eNAND]);
			fprintf(stdout, "   <numberOfDataInput: %d>\n", ((NAND *)sPtr)->numberOfDataInput);
			fprintf(stdout, "   <linkSymTable: %d>\n", ((NAND *)sPtr)->linkSymTable);
			fprintf(stdout, "   <ACTION>\n");
			// ((NAND *)sPtr)->linkSymTable(((NAND *)sPtr)->numberOfDataInput, -1);
			fprintf(stdout, "   </ACTION>\n");
			fprintf(stdout, "</%s>\n", cTagName[eNAND]);
			break;
		case eXOR:
			fprintf(stdout, "<%s>\n", cTagName[eXOR]);
			fprintf(stdout, "   <numberOfDataInput: %d>\n", ((XOR *)sPtr)->numberOfDataInput);
			fprintf(stdout, "   <linkSymTable: %d>\n", ((XOR *)sPtr)->linkSymTable);
			fprintf(stdout, "   <ACTION>\n");
			// ((XOR *)sPtr)->linkSymTable(((XOR *)sPtr)->numberOfDataInput, -1);
			fprintf(stdout, "   </ACTION>\n");
			fprintf(stdout, "</%s>\n", cTagName[eXOR]);
			break;
		case eMUX:
			fprintf(stdout, "<%s>\n", cTagName[eMUX]);
			fprintf(stdout, "   <numberOfDataInput: %d>\n", ((MUX *)sPtr)->numberOfDataInput);
			fprintf(stdout, "   <widthOfDataInput: %d>\n", ((MUX *)sPtr)->widthOfDataInput);
			fprintf(stdout, "   <linkSymTable: %d>\n", ((MUX *)sPtr)->linkSymTable);
			fprintf(stdout, "   <ACTION>\n");
			// ((MUX *)sPtr)->linkSymTable(((MUX *)sPtr)->numberOfDataInput, ((MUX *)sPtr)->widthOfDataInput);
			fprintf(stdout, "   </ACTION>\n");
			fprintf(stdout, "</%s>\n", cTagName[eMUX]);
			break;
		case eCLA:
			fprintf(stdout, "<%s>\n", cTagName[eCLA]);
			fprintf(stdout, "   <widthOfDataInput: %d>\n", ((CLA *)sPtr)->widthOfDataInput);
			fprintf(stdout, "   <linkSymTable: %d>\n", ((CLA *)sPtr)->linkSymTable);
			fprintf(stdout, "   <ACTION>\n");
			// ((CLA *)sPtr)->linkSymTable(((CLA *)sPtr)->numberOfDataInput, -1);
			fprintf(stdout, "   </ACTION>\n");
			fprintf(stdout, "</%s>\n", cTagName[eCLA]);
			break;
		default:
			// do nothing
			break;
		}
	}
}

// Prints AST for debugging
void AST::print(void)
{
	list.print(printListFunc);
}

//
// Create OR AST node
//
void AST :: insertOR (unsigned int numberOfDataInput)
{
	or = new OR ();
	or->linkSymTable = NULL;

	or->nodeType = eOR;
	or->numberOfDataInput = numberOfDataInput;
	or->linkSymTable = symT->getActionLink (eOR);

	list.append ((const char *)or);
}

//
// Create AND AST node
//
void AST :: insertAND (unsigned int numberOfDataInput)
{
	and = new AND ();
	and->linkSymTable = NULL;

	and->nodeType = eAND;
	and->numberOfDataInput = numberOfDataInput;
	and->linkSymTable = symT->getActionLink (eAND);

	list.append ((const char *)and);
}

//
// Create NAND AST node
//
void AST :: insertNAND (unsigned int numberOfDataInput)
{
	nand = new NAND ();
	nand->linkSymTable = NULL;

	nand->nodeType = eNAND;
	nand->numberOfDataInput = numberOfDataInput;
	nand->linkSymTable = symT->getActionLink (eNAND);

	list.append ((const char *)nand);
}

//
// Create EXOR AST node
//
void AST :: insertXOR (unsigned int numberOfDataInput)
{
	xor = new XOR ();
	xor->linkSymTable = NULL;

	xor->nodeType = eXOR;
	xor->numberOfDataInput = numberOfDataInput;
	xor->linkSymTable = symT->getActionLink (eXOR);

	list.append ((const char *)xor);
}

//
// Create MUX AST node
//
void AST :: insertMUX (unsigned int numberOfDataInput, unsigned int widthOfDataInput)
{
	mux = new MUX ();
	mux->linkSymTable = NULL;

	mux->nodeType = eMUX;
	mux->numberOfDataInput = numberOfDataInput;
	mux->widthOfDataInput = widthOfDataInput;
	mux->linkSymTable = symT->getActionLink (eMUX);

	list.append ((const char *)mux);
}

//
// Create CLA AST node
//
void AST :: insertCLA (unsigned int widthOfDataInput)
{
	cla = new CLA ();
	cla->linkSymTable = NULL;

	cla->nodeType = eCLA;
	cla->widthOfDataInput = widthOfDataInput;
	cla->linkSymTable = symT->getActionLink (eCLA);

	list.append ((const char *)cla);
}

// Returns the first node
const char * AST::getFirstNode (void)
{
	if(!list.isempty())
	{
		nPtr = list.getFirst ();
		return list.getData(nPtr);
	}
	else
		return NULL;
}

// Returns the first node
const char * AST::getNextNode (void)
{
	if(nPtr != NULL)
	{
		nPtr = list.getNext (nPtr);
		if(nPtr != NULL)
			return list.getData(nPtr);
		else
			return NULL;
	}
	else
		return NULL;
}
