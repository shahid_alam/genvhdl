/* ----------------------------------------------------------------------------
|
|   Filename:    ast.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for AST class to be included in AST and processXML
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _AST_H_
#define _AST_H_

#include "global.h"
#include "list.h"
#include "symtable.h"

namespace khudi
{
	/**
	*
	* This is the intermediate representation during translation 
	* from XML to the specified language in the form of an 
	* Abstract Syntax Tree (AST). It's is a linked list of XML 
	* structure. It contains a copy of XML with annotation i.e a 
	* link to a symbol table for actions to create code for 
	* specified component in a particular language. 
	* If traversed from bottom to top, and using the symbol table, 
	* it produces the source code for the specified 
	* language (VHDL / Verilog).
	*
	*/
	class AST : public List
	{
	public:
		/**
		*
		* Constructor
		*
		*/
		AST ();
		/**
		*
		* Destructor
		*
		*/
		~AST ();
		/**
		*
		* Create and insert OR node into AST
		* @param numberOfDataInput number of data input to the OR gate
		*
		*/
		void insertOR (unsigned int numberOfDataInput);
		/**
		*
		* Create and insert AND node into AST
		* @param numberOfDataInput number of data input to the AND gate
		*
		*/
		void insertAND (unsigned int numberOfDataInput);
		/**
		*
		* Create and insert NAND node into AST
		* @param numberOfDataInput number of data input to the NAND gate
		*
		*/
		void insertNAND (unsigned int numberOfDataInput);
		/**
		*
		* Create and insert XOR node into AST
		* @param numberOfDataInput number of data input to the XOR gate
		*
		*/
		void insertXOR (unsigned int numberOfDataInput);
		/**
		*
		* Create and insert MUX node into AST
		* @param numberOfDataInput number of data input to the MUX gate
		* @param widthOfDataInput width of data input to the MUX gate
		*
		*/
		void insertMUX (unsigned int numberOfDataInput, unsigned int widthOfDataInput);
		/**
		*
		* Create and insert CLA node into AST
		* @param numberOfDataInput number of data input to the MUX gate
		*
		*/
		void insertCLA (unsigned int widthOfDataInput);
		/**
		*
		* Print AST for debugging
		*
		*/
		void print (void);
		/**
		*
		* Returns a pointer to the first node.
		*
		*/
		const char * getFirstNode (void);
		/**
		*
		* Returns a pointer to the nex node.
		*
		*/
		const char * getNextNode(void);

	private:
		List list;
		void * nPtr;

		symTable *symT;

		OR *or;
		AND *and;
		NAND *nand;
		XOR *xor;
		MUX *mux;
		CLA *cla;

		void printTableIndex (long int index);
	};
}

#endif // _AST_H_