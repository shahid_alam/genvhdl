/* ----------------------------------------------------------------------------
|
|   Filename:    genCodeVHDL.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for genCodeVHDL.cpp
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _GENCODEVHDL_H_
#define _GENCODEVHDL_H_

#include "global.h"
#include "gencode.h"
#include "xmlstruct.h"

#define COMMENT_LINE_SIZE    60

/**
*
* Defining the data structure for buffer to store generated code
*
*/
typedef struct sStringBufCode
{
	char string[MAX_STRING_SIZE_CODE];
	long int size;
} tStringBufCode;

extern tStringBufCode stringBufCode;

/**
*
* VHDL header to be printed at the top of every entity in the library
*
*/
const char VHDLheader[] = 
							"library ieee;\n"
							"use ieee.std_logic_1164.all;\n\n";

/**
*
* Initialize local variables
*
*/
void init (void);
/**
*
* Clean the code buffer
*
*/
void clean (void);

#endif // _GENCODEVHDL_H_