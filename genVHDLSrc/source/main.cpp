/* ============================================================================
|
|   Filename:    main.cpp
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Main function for the program gencode. It instantiate an
|                XML reader (Xerces-C++: http://xerces.apache.org). Which
|                reads XML file and validate it agianst XML schema. The SAX
|                parser is used due to it's speed and lightness. The main
|                program uses processXML class to process the XML file. This
|                class during parsing calls genVHDL and AST classes to
|                process the XML. AST class creates a Link List of nodes
|                present in the XML file. Each node contains data for the node
|                and a link to the action function to generate code for that
|                node.
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/TransService.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>

#include "global.h"
#include "processXML.h"

/* ============================================================================

Local data

encodingName
   The encoding we are to output in. If not set on the command line,
   then it is defaulted to UTF-8.

xmlFile
   The path to the file to parser. Set via command line.

valScheme
   Indicates what validation scheme to use. It defaults to 'auto', but
   can be set via the -v= command.

-----------------------------------------------------------------------------*/
static char*                     fileNameVHDL       = "DCL.vhdl";
static char*                     xmlFile            = 0;
static SAX2XMLReader::ValSchemes valScheme          = SAX2XMLReader::Val_Auto;
static bool                      doSchema           = true;
static bool                      schemaFullChecking = true;
static bool                      doNamespaces       = true;
static bool                      namespacePrefixes  = false;


/* ============================================================================

	usage () function

PURPOSE:	Local helper methods. Prints the help file.
SETUP:		None.
PARAMETERS: None.
CALL:		usage ();
RETURN:		None.

-----------------------------------------------------------------------------*/
static void usage (void)
{
	printf("\nUsage:\n"
		"    genVHDL [options] <XML file>\n\n"
		"This program invokes the SAX2XMLReader, and then generate VHDL\n"
		"from the data returned by various SAX2 handlers for the specified\n"
		"XML file.\n\n"
		"Options:\n"
			"    -v=xxx		Validation scheme [always | never | auto*].\n"
			"    -o=xxx		Name of VHDL file to be created  [DCL.vhdl*].\n"
			"    -?			Show this help.\n\n"
			"  * = Default if not provided explicitly.\n\n");
}


/* ============================================================================

	main () function

PURPOSE:	Main function.
SETUP:		See function usage ().
PARAMETERS: argC: Number of arguments passed from the command line
            argV[]: Pointers to the arguments from the command line
CALL:		main (int, char **) See function usage ().
RETURN:		Zero when there is no error otherwise non zero.

-----------------------------------------------------------------------------*/
int main (int argC, char* argV[])
{
	// Initialize the XML system
	try
	{
		XMLPlatformUtils::Initialize ();
	}

	catch (const XMLException& toCatch)
	{
		printf("Error during initialization! :\n %s\n",
								XMLString::transcode(toCatch.getMessage()));
		return 1;
	}

	// Check command line and extract arguments.
	if (argC < 2)
	{
		usage();
		XMLPlatformUtils::Terminate ();
		return 1;
	}

	int parmN;
	for (parmN = 1; parmN < argC; parmN++)
	{
		// Break out on first parameter not starting with a dash
		if (argV[parmN][0] != '-')
		break;

		// Special case help request
		if (!strcmp(argV[parmN], "-?"))
		{
			usage();
			XMLPlatformUtils::Terminate();
			return 2;
		}
		else if (!strncmp(argV[parmN], "-v=", 3)
								||  !strncmp(argV[parmN], "-V=", 3))
		{
			const char* const parm = &argV[parmN][3];

			if (!strcmp (parm, "never"))
				valScheme = SAX2XMLReader::Val_Never;
			else if (!strcmp (parm, "auto"))
				valScheme = SAX2XMLReader::Val_Auto;
			else if (!strcmp (parm, "always"))
				valScheme = SAX2XMLReader::Val_Always;
			else
			{
				fprintf (stderr, "Unknown -v= value:  %s\n", parm);
				XMLPlatformUtils::Terminate();
				return 2;
			}
		}
		else if (!strncmp(argV[parmN], "-o=", 3)
								||  !strncmp(argV[parmN], "-O=", 3))
		{
			fileNameVHDL = &argV[parmN][3];
		}
		else
		{
			fprintf (stderr, "Unknown option '%s', ignoring it\n", argV[parmN]);
		}
	}

	//
	//  Filename parameter
	//
	if (parmN + 1 != argC)
	{
		usage ();
		XMLPlatformUtils::Terminate ();
		return 1;
	}
	xmlFile = argV[parmN];

	//
	//  Create a SAX parser object.
	//
	SAX2XMLReader* parser;
	SAX2XMLReader* reader = XMLReaderFactory::createXMLReader ();
	parser = reader;

    //
    //  Then, according to what we were told on
    //  the command line, set it to validate or not.
    //
	if (valScheme == SAX2XMLReader::Val_Auto)
	{
		parser->setFeature (XMLUni::fgSAX2CoreValidation, true);
		parser->setFeature (XMLUni::fgXercesDynamic, true);
	}

	if (valScheme == SAX2XMLReader::Val_Never)
	{
		parser->setFeature(XMLUni::fgSAX2CoreValidation, false);
	}

	if (valScheme == SAX2XMLReader::Val_Always)
	{
		parser->setFeature (XMLUni::fgSAX2CoreValidation, true);
		parser->setFeature (XMLUni::fgXercesDynamic, false);
	}


	parser->setFeature (XMLUni::fgSAX2CoreNameSpaces, doNamespaces);
	parser->setFeature (XMLUni::fgXercesSchema, doSchema);
	parser->setFeature (XMLUni::fgXercesSchemaFullChecking, schemaFullChecking);
	parser->setFeature (XMLUni::fgSAX2CoreNameSpacePrefixes, namespacePrefixes);

	//
	//  Create the handler object and install it as the document and error
	//  handler for the parser.
	//

	int errorCount = 0;
	int errorCode = 0;
	try
	{
		processXML *handler = new processXML (fileNameVHDL);
		parser->setContentHandler (handler);
		parser->setErrorHandler (handler);
		parser->parse (xmlFile);
		errorCount = parser->getErrorCount ();
	}
	catch (const OutOfMemoryException&)
	{
		printf("OutOfMemoryException\n");
		errorCode = 5;          
	}
	catch (const XMLException& toCatch)
	{
		fprintf (stderr, "\nAn error occurred\n  Error:  %s\n",
								XMLString::transcode (toCatch.getMessage ()));
		errorCode = 4;
	}

	if (errorCode) {
		XMLPlatformUtils::Terminate ();
		return errorCode;
	}

	//
	//  Delete the parser itself.  Must be done prior to calling Terminate, below.
	//
	delete reader;

	// And call the termination method
	XMLPlatformUtils::Terminate ();

	if (errorCount > 0)
		return 4;
	else
		return 0;
}
