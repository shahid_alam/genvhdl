/* ----------------------------------------------------------------------------
|
|   Filename:    processXML.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for processXML.cpp
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _PROCESSXML_H_
#define _PROCESSXML_H_

#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>

#include "global.h"
#include "xmlstruct.h"
#include "ast.h"
#include "genVHDL.h"

/**
*
* XERCESC namespace (Xerces-C++: http://xerces.apache.org). 
* Xerces-C++ is a validating XML parser written in a portable 
* subset of C++.
*
*/
using namespace xercesc;
/**
*
* Local Namespace to avoid any name conflicts and store all the classes 
* under one Namespace
*
*/
using namespace khudi;

/**
*
* It inherits an XML reader (Xerces-C++: http://xerces.apache.org) class. 
* Which reads XML file and validate it agianst XML schema. The SAX 
* parser is used due to it's speed and lightness. The main 
* program uses processXML class to process the XML file. This 
* class during parsing calls genVHDL and AST classes to 
* process the XML.
*
*/
class processXML : public DefaultHandler
{
public:
	/**
	*
	* Constructor
	*
	*/
    processXML ();
	/**
	*
	* Constructor
	* @param fileNameVHDL name of the VHDL file to be generated
	*
	*/
    processXML (char *fileNameVHDL);
	/**
	*
	* Destructor
	*
	*/
    ~processXML ();

	/**
	*
	* Event handler for end of XML document
	*
	*/
    void endDocument ();
	/**
	*
	* Event handler for end of XML element
	*
	*/
    void endElement ( const XMLCh* const uri,
					 const XMLCh* const localname,
					 const XMLCh* const qname);
	/**
	*
	* Event handler for handling text inside the tag
	*
	*/
    void characters (const XMLCh* const chars, const unsigned int length);
	/**
	*
	* Event handler for ignoring the white spaces
	*
	*/
    void ignorableWhitespace (const XMLCh* const chars, const unsigned int length);
	/**
	*
	* Event handler for start of XML document
	*
	*/
    void startDocument ();
	/**
	*
	* Event handler of start of XML element
	*
	*/
    void startElement (const XMLCh* const uri, const XMLCh* const localname,
                      const XMLCh* const qname, const Attributes& attributes);

	/**
	*
	* Event handler for handling warnings during parsing and validating XML document
	*
	*/
    void warning (const SAXParseException& exc);
	/**
	*
	* Event handler for handling errors during parsing and validating XML document
	*
	*/
    void error (const SAXParseException& exc);
	/**
	*
	* Event handler for handling fatal errors during parsing and validating XML document
	*
	*/
    void fatalError (const SAXParseException& exc);

private:
	char element[MAX_STRING_SIZE];
	char text[MAX_STRING_SIZE];
	char fileNameVHDL[MAX_FILE_NAME];
	int currentNode;

	OR or;
	AND and;
	NAND nand;
	XOR xor;
	MUX mux;
	CLA cla;

	AST *astHandler;
	genVHDL *genVHDLhandler;
};

#endif // _PROCESSXML_H_