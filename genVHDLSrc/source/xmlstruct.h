/* ----------------------------------------------------------------------------
|
|   Filename:    xmlstruct.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for XML structure to store AST nodes data
|                and link to the action function
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _XMLSTRUCT_H_
#define _XMLSTRUCT_H_

#include "gencode.h"

#define MAX_XML_ELEMENT_SIZE   128
#define MAX_NUMBER_NODES       256

/**
*
* Structure defining all the nodes in XML Schema
*
*/
const enum eNodeType
{
	eOR = 0,
	eAND,
	eNAND,
	eXOR,
	eMUX,
	eCLA
};

/**
*
* Tag names for XML nodes defined in XML Schema
*
*/
const char cTagName[MAX_NUMBER_NODES][MAX_XML_ELEMENT_SIZE] =
{
	"or",
	"and",
	"nand",
	"xor",
	"multiplexer",
	"cla"
};

/**
*
* Data structure for storing OR node
*
*/
typedef struct sOR
{
	unsigned int nodeType;
	unsigned int numberOfDataInput;
	tLinkSymTable linkSymTable;
} OR;

/**
*
* Data structure for storing AND node
*
*/
typedef struct sAND
{
	unsigned int nodeType;
	unsigned int numberOfDataInput;
	tLinkSymTable linkSymTable;
} AND;

/**
*
* Data structure for storing NAND node
*
*/
typedef struct sNAND
{
	unsigned int nodeType;
	unsigned int numberOfDataInput;
	tLinkSymTable linkSymTable;
} NAND;

/**
*
* Data structure for storing XOR node
*
*/
typedef struct sXOR
{
	unsigned int nodeType;
	unsigned int numberOfDataInput;
	tLinkSymTable linkSymTable;
} XOR;

/**
*
* Data structure for storing MUX (multiplexer) node
*
*/
typedef struct sMUX
{
	unsigned int nodeType;
	unsigned int numberOfDataInput;
	unsigned int widthOfDataInput;
	tLinkSymTable linkSymTable;
} MUX;

/**
*
* Data structure for storing CLA (carry look ahead adder) node
*
*/
typedef struct sCLA
{
	unsigned int nodeType;
	unsigned int widthOfDataInput;
	tLinkSymTable linkSymTable;
} CLA;

#endif // _XMLSTRUCT_H_