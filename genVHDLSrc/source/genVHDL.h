/* ----------------------------------------------------------------------------
|
|   Filename:    genVHDL.h
|   Dated:       26 February, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for genVHDL.cpp
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _GENVHDL_H_
#define _GENVHDL_H_

#include "global.h"
#include "ast.h"
#include "genCodeVHDL.h"

namespace khudi
{
	/**
	*
	* Class for generating VHDL code
	*
	*/
	class genVHDL
	{
	public:
		/**
		*
		* Constructor
		*
		*/
		genVHDL ();
		/**
		*
		* Constructor
		* @param fileName name of the file to be generated
		*
		*/
		genVHDL (const char *fileName);
		/**
		*
		* Destructor
		*
		*/
		~genVHDL ();
		/**
		*
		* Call the appropiate action function from the symbol table to generate code
		* @param astHandler pointer to AST
		*
		*/
		void processAST (AST *astHandler);

	private:
		FILE *filePointer;
		char stringBuf[MAX_STRING_SIZE];
		int stringBufSize;

		/**
		*
		* write the generated code from buffer to file
		*
		*/
		void writeFile ();
	};
}

#endif // _GENVHDL_H_